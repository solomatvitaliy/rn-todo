/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

// import { Platform, StyleSheet, import store from "./src/store";

// const instructions = Platform.select({
//   ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
//   android:
//     "Double tap R on your keyboard to reload,\n" +
//     "Shake or press menu button for dev menu"
// });
import React, { Component } from "react";
import { Provider } from "react-redux";
import ToDoApp from "./src/ToDoApp";
import store from "./src/store";

interface Props {}
interface State {
  name: string;
}

export default class App extends Component<Props, State> {
  render() {
    return (
      <Provider store={store}>
        <ToDoApp />
      </Provider>
    );
  }
}

// class Card extends Component<{ name: string }> {
//   render() {
//     return null;
//   }
// }
