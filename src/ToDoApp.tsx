import React from "react";
import { StyleSheet, View, Text } from "react-native";
import AddToDo from "./containers/AddToDo";
import VisibleToDos from "./containers/VisibleToDos";
import ToDosHandler from "./containers/ToDosHandler";
import ListCleaner from "./containers/ListCleaner";
import Title from "./components/Title";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#A1C2D8",
    paddingTop: 20
  }
});

const ToDoApp = () => {
  return (
    <View style={styles.container}>
      <Title />
      <AddToDo />
      <ToDosHandler />
      <VisibleToDos />
      <ListCleaner />
    </View>
  );
};

export default ToDoApp;
