/* eslint-disable no-plusplus */
import {
  ADD_TODO,
  TOGGLE_TODO,
  DELETE_TODO,
  SHOW_ALL_TODOS,
  SHOW_COMPLETED_TODOS,
  SHOW_UNCOMPLETED_TODOS,
  DELETE_ALL_TODOS
} from "./actionTypes";

let nextId = 0;

export const addToDo = (text: string) => ({
  type: ADD_TODO,
  // id: Math.random(),
  id: nextId++,
  text
});

export const toggleToDo = (id: number) => ({
  type: TOGGLE_TODO,
  id
});

export const deleteToDo = (id: number) => ({
  type: DELETE_TODO,
  id
});

export const showAllToDos = (completed: boolean) => ({
  type: SHOW_ALL_TODOS,
  completed
});

export const showCompletedToDos = (completed: boolean) => ({
  type: SHOW_COMPLETED_TODOS,
  completed
});

export const showUncompletedToDos = (completed: boolean) => ({
  type: SHOW_UNCOMPLETED_TODOS,
  completed
});

export const deleteAllToDos = (state: object) => ({
  type: DELETE_ALL_TODOS,
  state
});
