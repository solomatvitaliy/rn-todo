import React from "react";
import { StyleSheet, View, Text } from "react-native";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    paddingBottom: 10
    //     flex: 1,
    //     backgroundColor: "#A1C2D8",
    //     paddingTop: 50
  },
  title: {
    fontSize: 26,
    color: "black",
    fontStyle: "italic",
    fontWeight: "bold"
  }
});

const ToDoApp = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>ToDo App</Text>
    </View>
  );
};

export default ToDoApp;
