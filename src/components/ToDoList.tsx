import React from "react";
import {
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  Button
} from "react-native";

const styles = StyleSheet.create({
  todoList: {
    flex: 0,
    paddingHorizontal: 30
  }
});

export interface Todo {
  completed: boolean;
  id: number;
  text: string;
}

export interface ToDoListProps {
  todos: Array<Todo>;
  toggleToDo: any;
  deleteToDo: any;
}

const ToDoList: React.FC<ToDoListProps> = ({
  todos,
  toggleToDo,
  deleteToDo
}) => (
  <ScrollView style={styles.todoList}>
    {todos.map(todo => (
      <TouchableOpacity
        style={{
          flex: 0,
          flexDirection: "row",
          justifyContent: "space-between",
          borderColor: "black",
          borderWidth: 1,
          borderRadius: 4,
          marginBottom: 8,
          backgroundColor: todo.completed ? "#A0FA8E" : "#8ECFFA"
        }}
        key={todo.id}
        onPress={() => toggleToDo(todo.id)}
      >
        <Text
          style={{
            marginLeft: 20,
            fontSize: 24,
            textDecorationLine: todo.completed ? "line-through" : "none"
          }}
        >
          {todo.text}
        </Text>
        <Button
          title="Delete"
          color="crimson"
          onPress={() => deleteToDo(todo.id)}
        />
      </TouchableOpacity>
    ))}
  </ScrollView>
);

export default ToDoList;
