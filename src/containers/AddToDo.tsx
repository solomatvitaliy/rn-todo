/* eslint-disable react/destructuring-assignment */
import React, { Component } from "react";
import { StyleSheet, TextInput, View, Button } from "react-native";
import { connect } from "react-redux";
import { addToDo } from "../actions/index";

const styles = StyleSheet.create({
  form: {
    backgroundColor: "#fff",
    marginHorizontal: 20,
    flexDirection: "row",
    alignItems: "stretch"
  },
  input: {
    flex: 1,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 3,
    backgroundColor: "#6F95AF",
    paddingHorizontal: 10,
    height: 44,
    fontSize: 20
  }
});

class AddToDo extends Component {
  state = {
    text: ""
  };

  addToDo = (text: string) => {
    this.props.dispatch(addToDo(text));
    this.setState({ text: "" });
  };

  render() {
    return (
      <View style={styles.form}>
        <TextInput
          onChangeText={text => this.setState({ text })}
          value={this.state.text}
          style={styles.input}
          placeholder="Write what to do"
        />
        <Button
          onPress={() => this.addToDo(this.state.text)}
          color="purple"
          title="Add"
        />
      </View>
    );
  }
}

export default connect()(AddToDo);
