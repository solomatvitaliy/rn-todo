/* eslint-disable react/destructuring-assignment */
import React, { Component } from "react";
import { StyleSheet, View, Button } from "react-native";
import { connect } from "react-redux";
import { deleteAllToDos } from "../actions/index";

const styles = StyleSheet.create({
  toDosCleaner: {
    alignItems: "flex-start",
    padding: 20
  }
});

class ListCleaner extends Component {
  deleteAllToDos = () => {
    this.props.dispatch(deleteAllToDos());
  };

  render() {
    return (
      <View style={styles.toDosCleaner}>
        <Button
          onPress={() => this.deleteAllToDos()}
          color="navy"
          title="Delete All Todos"
        />
      </View>
    );
  }
}

export default connect()(ListCleaner);
