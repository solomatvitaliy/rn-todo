/* eslint-disable react/destructuring-assignment */
import React, { Component } from "react";
import { StyleSheet, View, Button } from "react-native";
import { connect } from "react-redux";
import {
  showAllToDos,
  showCompletedToDos,
  showUncompletedToDos
} from "../actions/index";

const styles = StyleSheet.create({
  toDosHandler: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
    padding: 20
  }
});

class ToDosHandler extends Component {
  showAllToDos = () => {
    this.props.dispatch(showAllToDos());
  };
  showCompletedToDos = () => {
    this.props.dispatch(showCompletedToDos());
  };
  showUncompletedToDos = () => {
    this.props.dispatch(showUncompletedToDos());
  };

  render() {
    return (
      <View style={styles.toDosHandler}>
        {/* <Button
          onPress={() => this.showAllToDos()}
          color="navy"
          title="All ToDos"
        /> */}
        <Button
          onPress={() => this.showCompletedToDos()}
          color="navy"
          title="Done ToDos"
        />
        <Button
          onPress={() => this.showUncompletedToDos()}
          color="navy"
          title="Not Done ToDos"
        />
      </View>
    );
  }
}

export default connect()(ToDosHandler);
