import { connect } from "react-redux";
import ToDoList from "../components/ToDoList";
import { toggleToDo, deleteToDo, handleToDos } from "../actions/index";

const mapStateToProps = (state: any) => ({
  todos: state.todos
});

const mapDispatchToProps = (dispatch: any) => ({
  toggleToDo: (id: number) => dispatch(toggleToDo(id)),
  deleteToDo: (id: number) => dispatch(deleteToDo(id))
  // handleToDos: (completed: boolean) => dispatch(handleToDos(completed))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToDoList);
