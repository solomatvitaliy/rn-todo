const todos = (state = [], action: any): any => {
  switch (action.type) {
    case "ADD_TODO":
      if (action.text !== "") {
        return [
          ...state,
          {
            id: action.id,
            text: action.text,
            completed: false
          }
        ];
      } else {
        alert("You must type something");
      }

    case "TOGGLE_TODO":
      return state.map(todo =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );

    case "DELETE_TODO":
      const todos = state.filter(el => {
        return el.id !== action.id;
      });
      return todos;

    case "SHOW_ALL_TODOS":
      console.log("show all todos");
      return state;

    case "SHOW_COMPLETED_TODOS":
      console.log("show completed todos");
      const completedTodos = state.filter(el => {
        return el.completed === true;
      });
      if (completedTodos.length == 0) {
        return state;
      } else {
        return completedTodos;
      }

    case "SHOW_UNCOMPLETED_TODOS":
      console.log("show uncompleted todos");
      const uncompletedTodos = state.filter(el => {
        return el.completed === false;
      });
      if (uncompletedTodos.length == 0) {
        return state;
      } else {
        return uncompletedTodos;
      }

    case "DELETE_ALL_TODOS":
      console.log("all todos deleted");
      return (state = []);

    default:
      return state;
  }
};

export default todos;
