/* eslint-disable no-underscore-dangle */
import logger from "redux-logger";
import { compose, applyMiddleware, createStore } from "redux";
import rootReducer from "../reducers";

// eslint-disable-next-line no-undef
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// eslint-disable-next-line no-undef
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(logger))
);

export default store;
